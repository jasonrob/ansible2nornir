# ansible2nornir

Tool to convert ansible inventory to nornir inventory.  

## Getting started

From your ansible controller, run the following:

```
ansible-inventory --export --list | ansible2nornir
```

Ensure you edit the `nornir_inventory_path` variable to your nornir inventory path.

## Nornir inventory

To install nornir inventory in your home folder:

```
mkdir -p $HOME/nornir/inventory
touch $HOME/nornir/config.yaml
touch $HOME/nornir/inventory/defaults.yaml
```

Contents of `config.yaml`:

```
---
runner:
  plugin: threaded
  options:
    num_workers: 10
inventory:
  plugin: SimpleInventory
  options:
    host_file: "inventory/hosts.yaml"
    group_file: "inventory/groups.yaml"
    defaults_file: "inventory/defaults.yaml"
```